<?php
function _get_datatables_query($data,$count=FALSE)
    {
		global $db;
        $table          = $data["table"];
        $column_order   = $data["column"]["order"];
        $column_search  = $data["column"]["search"];
        $order          = $data["order"];
        $where          = $data["where"];

		if($count)
		{
			$query ="SELECT count(*) as count  FROM $table";
		}else{
			$query =db_from($table);
		}
        //$query ="SELECT * FROM $table";
        //$query .=db_where();

		if($where!='')
		{
		$query .=" WHERE  $where ";
		}
        $i = 0;
       foreach ($column_search as $items) 
        {
            if(isset($_POST['search']['value']) && $_POST['search']['value']!='')
            {
				$search=$_POST['search']['value'];

                if($i===0)
                {
                    //$query .=db_like($item, $_POST['search']['value']);
					if($where!='')
					{
                    //$query .=" WHERE  $where ";
					$query .=" AND (  `$items` LIKE '%$search%' ";

					}else{
					$query .=" WHERE  ( `$items` LIKE '%$search%' ";

					}
                }
                else
                {
                   //$query .=or_like($item, $_POST['search']['value']);
					$query .=" OR  `$items` LIKE '%$search%' ";
			   }
                if(count($column_search) - 1 == $i) {
					//$query .=db_like($item, $_POST['search']['value']);
					$query .=")";

				}
            }else{
			$query .="  ";

			}
            $i++;
        }
        if(!empty($_POST['order']['0']['column']) ) 
        {
			$query .=" ORDER BY  `".$column_order[$_POST['order']['0']['column']]."` ".$_POST['order']['0']['dir']." ";

        }
        else
        {
			$query .=" ORDER BY  `".$order[0]."` ".$order[1]." ";

        }
		
		return $query;
    }
	function get_datatables($data)
    {
		global $db, $database_engine;

        $query = _get_datatables_query($data);
		$column=$data["column"]["search"];
        if($_POST['length'] != -1)
		{
			$query .= db_limit(intVal($_POST['start']),intVal($_POST['length']));

		}
		$queries=$db->query($query);
        $items= array();
        if($database_engine == 'sqlite')
        {
            while($row=$queries->fetchArray()){
                foreach($column as $nama=>$val){
                $col[$val]=$row[$val];
                }			
                $items[] = $col;
            }	
        }
        elseif($database_engine == 'mysql'){
               while($row=mysql_fetch_array($queries)){
                for($i=0;$i<count($column);$i++){
                $col[$column[$i]]=$row[$column[$i]];
                }			
                $items[] = $col;
            }	
        }
        elseif($database_engine == 'mysqli'){
               while($row=mysqli_fetch_array($queries)){
                for($i=0;$i<count($column);$i++){
                $col[$column[$i]]=$row[$column[$i]];
                }			
                $items[] = $col;
            }	
        }

			return $items;
    }
   
    function datatables_count_filtered($data)
    {
		global $db;
		$query = _get_datatables_query($data,TRUE);
        $table=$data["table"];
        return get_num_rows($query);

	}
   
    function datatables_count_all($data)
    {
		global $db;
        $table=$data["table"];
        return checkData($table);
    }

	function doDatatables($table,$fields,$where="",$order_by,$setting="")
	{
        $table=$table;
        $fields=$fields;
        $search=$fields;
        $order=$fields;
        $datatables=array(
            "table"=>$table,
            "column"=>array(
                "order"=>$order,
                "search"=>$search,
                ),
            "order" => $order_by,
            "where" => $where,
            );
		$list = get_datatables($datatables);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $record) {
            $no++;
            $row = array();
            foreach($fields as $value)
            {
				if(in_array($value,$setting))
				{
                 $row[] = currency($record[$value]); 
				}else{
                 $row[] = $record[$value]; 
				}
            }
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => datatables_count_all($datatables),
            "recordsFiltered" =>datatables_count_filtered($datatables),
            "data" => $data,
        );
        echo json_encode($output);
		
	}
