<?php 
/* APLIKASI PENJUALAN DPOS PRO
 *
 * Framework DPOS BISNIS berbasis PHP
 *
 * Developed by djavasoft.com
 * Copyright (c) 2018, Djavasoft Smart Technology
 *
 * @author	Mohamad Anton Arizal, S.T
 * @copyright	Copyright (c) 2018 Djavasoft. (https://djavasoft.com/)
 *
 *
*/

ob_start();
session_start() ;
include'config.php';
$dir=$APP_DIR.'/action';
$getDir=dirToArray($dir);

if(isset($_GET['action'])){
	
	include $dir.'/'.$_GET['action'].'.action.php';

}else{
	
foreach($getDir as $d)
{
	include $dir.'/'.$d;
}

}


?>

