<?php 
/* APLIKASI PENJUALAN DPOS PRO
 *
 * Framework DPOS BISNIS berbasis PHP
 *
 * Developed by djavasoft.com
 * Copyright (c) 2018, Djavasoft Smart Technology
 *
 * @author	Mohamad Anton Arizal, S.T
 * @copyright	Copyright (c) 2008 Djavasoft. (https://djavasoft.com/)
 *
 *
*/

date_default_timezone_set("Asia/Jakarta");
$CORE_URL			= "http://localhost/kasir";
$SID				= "";
$PORT				= "80";
$SNAME				= "DPOS TOKO PRO";
$version			= file_get_contents("app/version.txt");
$database_engine	="mysqli"; 					//Anda bisa mengganti dengan sqlite atau mysql


/*------------------------------------ DATABASE MYSQL ------------------------------*/

if($database_engine=="mysql"){
	//MENGGUNAKAN MYSQL
	include 			"libraries/Database/mysql.class.php";	//class mysql database
	$db 				= new Database();			// instance database
	$db->dbHost			="localhost";				// database host
	$db->dbUser			="root";					// database username
	$db->dbPass			=""; 						// dabatabse password
	$db->dbName			="kasirku"; 				// database name
	include 			"libraries/Database/mysql.db.php";		// fungsi mysql


}/*------------------------------------ DATABASE MYSQLi ------------------------------*/

elseif($database_engine=="mysqli"){
	//MENGGUNAKAN MYSQL
	include 			"libraries/Database/mysqli.class.php";	//class mysql database
	$db 				= new Database();			// instance database
	$db->dbHost			="localhost";				// database host
	$db->dbUser			="root";					// database username
	$db->dbPass			=""; 						// dabatabse password
	$db->dbName			="kasirku"; 				// database name
	include 			"libraries/Database/mysqli.db.php";		// fungsi mysql

/*------------------------------------ DATABASE SQLITE ------------------------------*/

}elseif($database_engine=="sqlite"){
	//MENGGUNAKAN SQLITE3
	$storagelocation 	= __DIR__ ."/"; 						//direktori root
	$db_file 			= $storagelocation."/db/database.db";	//direktori menyimpan file database sqlite
	$db					= new SQLite3($db_file);				// instance database
	include 			"libraries/Database/sqlite.db.php";				// fungsi sqlite
}

/*------------------------------------ LIBRARIES ------------------------------*/

$dir = "libraries/Core/*";
foreach(glob($dir) as $file) 
{
	include $file;
}


$dir = "libraries/Dpos/*";
foreach(glob($dir) as $file) 
{
	include $file;
}


include "libraries/Helper/ox000fx.inc";

/*------------------------------------ APP ------------------------------*/

$APP_DIR= __DIR__."/app";
$ASSETS_DIR= __DIR__."/assets";
?>
