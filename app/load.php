<?php 
/* APLIKASI PENJUALAN DPOS PRO
 *
 * Framework DPOS BISNIS berbasis PHP
 *
 * Developed by djavasoft.com
 * Copyright (c) 2018, Djavasoft Smart Technology
 *
 * @author	Mohamad Anton Arizal
 * @copyright	Copyright (c) 2018 Djavasoft. (https://djavasoft.com/)
 *
 *
*/



ob_start();
session_start();
$username=$_SESSION['user'];
$userlevel=userLevel($username);
notif();
include __DIR__."/page/system.php";	//break;

if(isset($_GET['mode'])){

$mode=$_GET['mode'];
$mode_file=$_GET['mode'].'.php';
if(file_exists(__DIR__.'/main/'.$mode.'/'.$mode_file))
{
	if(file_exists(__DIR__.'/main/'.$_GET['mode'].'/'.$_GET['mode'].'.js.php'))
	{
		include (__DIR__.'/main/'.$_GET['mode'].'/'.$_GET['mode'].'.js.php');
	}
	if(file_exists(__DIR__.'/main/'.$_GET['mode'].'/'.$_GET['mode'].'.modal.php'))
	{
		include (__DIR__.'/main/'.$_GET['mode'].'/'.$_GET['mode'].'.modal.php');
	}
	
	include(__DIR__.'/main/'.$mode.'/'.$mode_file);

}
else
{

	echo '<div class="col-md-12"><div class="alert alert-danger"><i class="fa fa-warning"></i> File not found ('.__DIR__.'app/main/'.$mode.')</div></div>';
}
}
else
{
	include 'main/dashboard.php';

}
?>