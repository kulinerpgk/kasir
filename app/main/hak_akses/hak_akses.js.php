<?php
function mysqli_field_name($result, $field_offset)
{
    $properties = mysqli_fetch_field_direct($result, $field_offset);
    return is_object($properties) ? $properties->name : null;
}


if($database_engine=='sqlite'){
?>

<script>

function saveHakAkses(levels, formName, inputAksesName) {
    <?php
$results=$db->query("SELECT * FROM 'akses' ");
$cols = $results->numColumns(); 
for ($i = 0; $i < $cols; $i++) {
echo "var ".$results->columnName($i)."=$('input[name=".$results->columnName($i)."]:checked', '#'+formName).val();";
}

?>
    $.get("data.php?inputHakAkses=" + levels +
        <?php
$results=$db->query("SELECT * FROM 'akses' ");
$cols = $results->numColumns(); 
for ($i = 0; $i < $cols; $i++) {
echo '"&'.$results->columnName($i).'="+'.$results->columnName($i).'+';
}
?> "&data=1",
        function(data) {
            swal({
                title: 'Sukses!',
                text: 'Data berhasil diupdate' + data,
                type: 'success',
                timer: 2000
            });
        }
    );
}
$(document).ready(function() {

    <?php 
$aksesList=doTableArray("akses",array("id","level"));
foreach( $aksesList as $list){
	$lvl=$list[1];
	$level=ucwords($list[1]);
echo '
$( "#save'.$level.'" ).click(function () {
saveHakAkses("'.$lvl.'","form'.$level.'","inputAkses'.$level.'");
} );
';
}
?>
});
</script>


<?php
/*======================================================= MYSQL DB ==========================================================*/ 
}elseif($database_engine=='mysql'){
?>

<script>
function saveHakAkses(levels, formName, inputAksesName) {
    <?php
global $db;
$results=$db->query("SELECT * FROM akses ");



		$cols = mysql_num_fields($results);

		for ($i = 0; $i < $cols; $i++) {
		echo "var ".mysql_field_name($results, $i)."=$('input[name=".mysql_field_name($results, $i)."]:checked', '#'+formName).val();";
		}

		?>
    $.get("data.php?inputHakAkses=" + levels +
			<?php
		$results=$db->query("SELECT * FROM akses ");
		$cols = mysql_num_fields($results);
		for ($i = 0; $i < $cols; $i++) {
		echo '"&'.mysql_field_name($results, $i).'="+'.mysql_field_name($results, $i).'+';
		}




?> "&data=1",
        function(data) {
            swal({
                title: 'Sukses!',
                text: 'Data berhasil diupdate',
                type: 'success',
                timer: 2000
            });
        }
    );
}
$(document).ready(function() {
    <?php 
$aksesList=doTableArray("akses",array("id","level"));
foreach( $aksesList as $list){
	$lvl=$list[1];
	$level=ucwords($list[1]);
echo '
$( "#save'.$level.'" ).click(function () {
saveHakAkses("'.$lvl.'","form'.$level.'","inputAkses'.$level.'");
} );
';
}
?>
});
</script>
<?php
/*======================================================= MYSQLi DB ==========================================================*/ 

}elseif($database_engine=='mysqli'){
?>

<script>
function saveHakAkses(levels, formName, inputAksesName) {
    <?php
global $db;
$results=$db->query("SELECT * FROM akses ");


		$cols = mysqli_num_fields($results);

		for ($i = 0; $i < $cols; $i++) {
		echo "var ".mysqli_field_name($results, $i)."=$('input[name=".mysqli_field_name($results, $i)."]:checked', '#'+formName).val();";
		}

		?>
    $.get("data.php?inputHakAkses=" + levels +
			<?php
		$results=$db->query("SELECT * FROM akses ");
		$cols = mysqli_num_fields($results);
		for ($i = 0; $i < $cols; $i++) {
		echo '"&'.mysqli_field_name($results, $i).'="+'.mysqli_field_name($results, $i).'+';
		}	


?> "&data=1",
        function(data) {
            swal({
                title: 'Sukses!',
                text: 'Data berhasil diupdate',
                type: 'success',
                timer: 2000
            });
        }
    );
}
$(document).ready(function() {
    <?php 
$aksesList=doTableArray("akses",array("id","level"));
foreach( $aksesList as $list){
	$lvl=$list[1];
	$level=ucwords($list[1]);
echo '
$( "#save'.$level.'" ).click(function () {
saveHakAkses("'.$lvl.'","form'.$level.'","inputAkses'.$level.'");
} );
';
}
?>
});
</script>
<?php
}
?>
<script>
$(document).ready(function() {
    $("#tambah_akses").click(function() {
        $("#EditAkses").modal("show");

    });
    var ajaxAkses = "data.php?tableAkses=akses";
    var tableAkses = $('#tableAkses').DataTable({
        "language": {
            "emptyTable": "No data available in table"
        },
        scrollY: '50vh',
        scrollX: true,
        scrollCollapse: false,
        "bSort": false,
        "pageLength": 1000,
        "lengthMenu": [1000, 2000],
        "paginate": false,
        "bFilter": true,
        "info": false,
        "bLengthChange": false,
        "ajax": ajaxAkses,
        "order": [
            [0, "desc"]
        ],
        "columnDefs": [{
                "targets": -1,
                "data": null,
                "defaultContent": "<div class='pull-right' style='width:50px'><button class='btn btn-danger btn-xs ' id='deleteAkses'><i class='fa fa-trash-o'></i></button></div>"
            },
            {
                "targets": [1],
                "render": function(data, type, row) {
                    return '<a href="#" id="addSatuan"> ' + data + ' </a>';
                },
            },
            {
                "targets": [0],
                "visible": false,
                "searchable": false
            }
        ]
    });
    $('#tableAkses tbody').on('click', '#deleteAkses', function() {
        var data = tableAkses.row($(this).parents('tr')).data();
        $.get("data.php?deleteAkses=" + data[0],
            function(data) {
                $("#EditAkses").modal("hide");
                $("#loadBody").load("load.php?mode=hak_akses");

            });
    });
    $("#insertAkses").click(function() {
        var level = $('#inputAkses').val();
        $.get("data.php?inputAkses=akses&level=" + level,
            function(data) {
                $("#EditAkses").modal("hide");
                $("#loadBody").load("load.php?mode=hak_akses");
            }
        );
    });
});
</script>




