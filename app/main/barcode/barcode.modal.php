<!-- Barang -->

<div class="modal fade" id="doBarang" tabindex="-1" role="dialog" aria-labelledby="EditPostLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h5 class="modal-title" id="EditPostLabel"><i class="fa fa-check-square-o" aria-hidden="true"></i> Data
                    Barang</h5>

            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover " id="tableBarang" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th style="width:50px">ID</th>
                                <th>Kode</th>
                                <th>Nama Barang</th>
                                <th>Harga Jual</th>
                                <th>Stok</th>
                                <th style="width:20px">#</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>