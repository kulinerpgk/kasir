<?php

if(trim($xdata)==getPengaturan('aktivasi') || 
(HDDLabel()!='' AND numHash(hexdec(HDDLabel()))==getPengaturan('aktivasi')) ||
(trim(getPengaturan('serial'))!='' AND numHash(getPengaturan('serial'))==trim(getPengaturan('aktivasi')))
){
	
}
?>

<script>
function playAddCart() {
    document.getElementById('playAddCart').play();
}

function playTrash() {
    document.getElementById('playTrash').play();
}
</script>
<style>
#tableBarang_filter {
    display: none !important
}
</style>
<audio id="playAddCart" src="<?php echo $CORE_URL;?>/app/sound/addcart.mp3"></audio>
<audio id="playTrash" src="<?php echo $CORE_URL;?>/app/sound/trash.mp3"></audio>

<div class="col-md-7">
    <div class="box box-solid">
        <div class="box-body" id="grid-mode">
            <div class="row">
                <div class="col-md-6" style="display:">
                    <form id="addCartForm" action="" method="post" style="display:">
                        <div class="input-group" style="display:">
                            <span class="input-group-btn"><button style="background:white;border: 1px solid #ccc"
                                    class="btn " type="button"><i class="fa  fa-barcode"></i> </button></span>
                            <input class="form-control" type="text" id="kodeBarang" autofocus title="kode barang"
                                name="kode_barang" autocomplete=OFF placeholder="Kode Produk">
                            <span class="input-group-btn" style="display:none">
                                <button class="btn " type="button" id="showBarang" title="cari barang atau tekan F1"><i
                                        class="fa  fa-search" aria-hidden="true"></i> [F1]</button>
                            </span>
                        </div>

                        <span style="display:none">
                            <input type="hidden" name="addCart" value="kasir_penjualan">
                            <input type="hidden" name="user_id" value="<?php echo userID($_SESSION['user']);?>"
                                placeholder="user_id">
                            <input type="hidden" name="faktur" value="<?php echo getFakturID('','PJ');?>"
                                placeholder="faktur">

                            <input style="display:none;margin-top:5px" class="form-control" type="text" id="idBarang">
                            <input style="display:block;margin-top:5px" class="form-control" type="text" id="namaBarang"
                                readonly placeholder="Nama Barang">
                            <input style="display:block;margin-top:5px" class="form-control" type="text" id="hargaJual"
                                readonly placeholder="Harga">
                            <input style="display:block;margin-top:5px;display:none" class="form-control" type="text"
                                id="stokBarang" readonly placeholder="Stok">
                            <div class="input-group" style="margin-top:5px">
                                <span class="input-group-btn"><button class="btn " type="button"
                                        style="width:111px">Jumlah </button></span>
                                <input style="text-align:center" id="qty" name="qty" type="number" value="1"
                                    class="form-control " title="jumlah barang">
                                <span class="input-group-btn">
                                    <input type="submit" value="Tambah" class="btn btn-primary" style="display:none" />
                                    <span class="input-group-btn"><button class="btn btn-warning" type="button"
                                            id="addCart"><i class="fa  fa-check-square-o" aria-hidden="true"></i>
                                            Tambah</button></span>
                                </span>
                        </span>
                </div>
                </form>
            </div>

            <div class="col-md-6" style="display:">
                <div class="input-group" style="display:">
                    <span class="input-group-btn"><button style="background:white;border: 1px solid #ccc" class="btn "
                            type="button"><i class="fa  fa-search"></i> </button></span>
                    <input class="form-control" type="text" id="cariBarang" autofocus title="Cari Barang"
                        name="kode_barang" autocomplete=OFF placeholder="Search">
                    <span class="input-group-btn" style="display:none">
                        <button class="btn " type="button" id="showBarang" title="cari barang atau tekan F1"><i
                                class="fa  fa-search" aria-hidden="true"></i> [F1]</button>
                    </span>
                </div>
            </div>
        </div>


    </div>
    <div class="box-body">
        <div class="table-responsive" id="list-mode">
            <table class="table table-bordered table-hover " id="tableBarang" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th style="width:50px">ID</th>
                        <th>Kode</th>
                        <th>Nama_Produk</th>
                        <th>Harga_Jual</th>
                        <th>Stok</th>
                        <th style="width:20px">#</th>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="well well-sm">
            F1 : Barcode | F2 : Search | END : Bayar
        </div>
    </div>
</div>

</div>
<div class="col-md-5">
    <div class="box box-solid">
        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-bordered table-hover " id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th style="width:20px">ID</th>
                            <th>Kode</th>
                            <th>Nama</th>
                            <th>Harga </th>
                            <th style="width:80px">Qty</th>
                            <th>Sub</th>
                            <th style="width:30px"></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
        <div class="box-body">
            <div style="display:block;margin-top:5px">
                <span id="totalSum_"></span>
                <span id="SubTotal"></span>
                <span id="totalSumBayar" style="display:none"></span>
            </div>
            <div style="display:none;margin-top:5px">
                <div class="input-group">
                    <span class="input-group-btn"><button class="btn " type="button" style="padding-right:50px">Faktur
                        </button></span>
                    <input class="form-control" type="text" id="faktur" value="<?php echo $faktur;?>" readonly>
                </div>
            </div>



            <p class="pull-right">
                <a href="#" class='btn btn-primary' id='bayarKasir' style="margin-top:10px;width:100%"><i
                        class='fa fa-print'></i> Bayar </a>
            </p>
        </div>
    </div>
</div>