<?php if($userlevel!='master'){echo'<style>#hapusTransaksi{display:none}</style>';};?>

<div class="modal fade" id="modalPenjualan" tabindex="-1" role="dialog" aria-labelledby="EditPostLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header" style="display:none">
                <h5 class="modal-title" id="EditPostLabel">Transaksi Penjualan</h5>
            </div>
            <div class="modal-body">
                <hr>
                <div id="showTablePenjualan"></div>
            </div>
            <div class="modal-footer">
                <a class="btn btn-primary" href="#" id="hapusTransaksi"><i class="fa fa-trash" aria-hidden="true"></i>
                    Hapus </a>

                <a class="btn btn-primary" onclick="jQuery('#printArea').print()" href="#" id="printPenjualan"><i
                        class="fa fa-print" aria-hidden="true"></i> Print</a>
                <a class="btn btn-primary" href="#" id="cetakStruk"><i class="fa fa-print" aria-hidden="true"></i> Cetak
                    Struk</a>
                <button class="btn btn-danger" type="button" data-dismiss="modal"><i class="fa fa-window-close"
                        aria-hidden="true"></i> Tutup</button>
            </div>
        </div>
    </div>
</div>

<!--- PRINT -->
<div class="modal " id="modalPrint" tabindex="-1" role="dialog" aria-labelledby="EditPostLabel" aria-hidden="true">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header modal-header-primary">
                <h5 class="modal-title" id="EditPostLabel"><i class="fa fa-address-card-o " aria-hidden="true"></i>
                    Transaksi </h5>
            </div>
            <div class="modal-body">
                <div style="overflow-y: auto; height:350px; ">
                    <div id="dataPrint"></div>
                    <iframe id="frame" src="" width="100%" height="300" style="border:none"> </iframe>
                </div>

                <center>
                    <div class="well well-sm" style="display:block">
                        Ukuran Kertas <select id="ukuran">
                            <option value="58">58 mm</option>
                            <option value="80">80 mm</option>
                            <option value="A4">A4 Responsive</option>
                        </select>
                        <div style="display:none">
                            Font <select id="font_family">
                                <option>Arial</option>
                                <option>Verdana</option>
                                <option>Tahoma</option>
                                <option>Courier</option>
                                <option>Trebuchet MS</option>
                            </select>
                            <select id="font_size">
                                <option>8</option>
                                <option>9</option>
                                <option>10</option>
                                <option selected>11</option>
                                <option>12</option>
                                <option>13</option>
                                <option>14</option>
                                <option>15</option>
                            </select>
                        </div>
                    </div>
                    <a class="btn btn-primary btn-sm" href="#" id="printKasir"><i class="fa fa-print"
                            aria-hidden="true"></i> Cetak Struk</a>
                    <button class="btn btn-danger" type="button" data-dismiss="modal"><i class="fa fa-window-close"
                            aria-hidden="true"></i> Batal</button>

                </center>
                <div style="display:none">
                    <iframe src="#" name="frame" id="printFrame"></iframe>
                </div>
            </div>
        </div>
    </div>
</div>