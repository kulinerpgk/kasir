
<script src="<?php echo $CORE_URL;?>/assets/plugins/air-datepicker/js/datepicker.min.js"></script>
<script src="<?php echo $CORE_URL;?>/assets/plugins/air-datepicker/js/i18n/datepicker.id.js"></script>

<script>
var ajaxBarang="data.php?kasirBarang=daftar_barang&jenisBarang=barang";
var ajaxData="data.php?penjualan_per_barang=kasir_penjualan";

$(document).ready(function() {
    var table = $('#dataTable').DataTable( {
    "language": {
      "emptyTable": "No data available in table"
    },
			"footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
 
            // Total over all pages
            qty = api
                .column( 6 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

 
            // Update footer
            $( api.column( 6 ).footer() ).html(
                ''+ formatNumber(qty) +''
            );
			$("#getItem").html(
                '<b>'+ formatNumber(qty) +'</b>'
            );
            total = api
                .column( 7 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

 
            // Update footer
            $( api.column( 7 ).footer() ).html(
                ''+ formatNumber(total) +''
            );
			$("#getTotal").html(
                '<b> Rp.'+ formatNumber(total) +'</b>'
            );

        },	
		dom: 'Bfrtip',			
		responsive: true,
		"paginate":true,
		"bFilter":true,
		"info":false,
		"bLengthChange": false,
        "ajax": ajaxData ,
		dom: 'Bfrtip',		
		responsive: true,
        buttons: [
            { extend: 'copyHtml5', footer: true },
            { extend: 'excelHtml5', footer: true },
            { extend: 'csvHtml5', footer: true },
            { extend: 'pdfHtml5', footer: true }
			],
		"order": [[ 0, "desc" ]],
        "columnDefs": [ ]
    } );
	
	
	
$('#kodeBarang').focus();
var tableBarang = $('#tableBarang').DataTable( {
"language": {
"emptyTable": "No data available in table"
},

select: true,
keys: true,
"pageLength": 8,
"lengthMenu": [ 5,10, 25, 50 ],
"paginate":true,
"bLengthChange": false,
"info":false,
"length":false,
"serverSide": false,
"ajax": {
    "url": ajaxBarang,
    "type": "GET"
},
"order": [[ 0, "desc" ]],

"columnDefs": [ {
"targets": -1,
"data": null,
"defaultContent": "<a href='#' class='btn btn-warning btn-sm' id='addBarang'><i class='fa fa-check-square-o'></i> pilih</a>"
},
{
"targets": [ 0 ],
"visible": false,
"searchable": false
}
]
} );
tableBarang
.on('key-focus', function (e, datatable, cell) {
datatable.rows().deselect();
datatable.row( cell.index().row ).select();
});
$('#tableBarang tbody').on( 'click', '#addBarang', function () {
var data = tableBarang.row( $(this).parents('tr') ).data();
var prt="'#printBarcode'";
var barcodes='<div id="printBarcode"><div style="height:100px;width:220px;margin:5px;border:1px solid #ccc;padding:5px"><center>'+data[2]+'<br><img  src="page.php?page=barcode&codetype=Code39&size=28&print=true&text='+data[ 1 ]+'"></div></div><br><a class="btn btn-primary" onclick="jQuery('+prt+').print()" href="#" id="printPenjualan"><i class="fa fa-print" aria-hidden="true"></i> Cetak Barcode</a>';
$('#idBarang').val(data[ 0 ]);
$('#kodeBarang').val(data[ 1 ]);
$('#namaBarang').val(data[ 2 ]);
$('#hargaJual').val(data[ 3 ]);
$('#stokBarang').val(data[ 4 ]);
$('#doBarang').modal('hide');
$('#loadBarcode').html(barcodes);
$('#qty').focus();
$('#qty').select();
} );
$( "#showBarang" ).click(function () {
$('#doBarang').modal('show');
tableBarang.ajax.url( ajaxBarang ).load();
} );

$( "#filter1" ).click(function () {
	
	var kodeBarang=$('#kodeBarang').val();
	var startDate=$('#startDate').val();
	var endDate = $('#endDate').val();
	var status=$('#status :selected').val();
	var user_id=$('#user_id :selected').val();
//	alert(lapak_id)
var getTitle = $(document).attr('title');
document.title ='Penjualan | <?php echo getPengaturan('nama_toko');?> ('+ startDate.replace("/","-").replace("/","-") +' - ' +endDate.replace("/","-").replace("/","-") +')';
	table.ajax.url( "data.php?filter_penjualan_per_barang=kasir_penjualan&startDate="+startDate+"&endDate="+endDate+"&kode_barang="+kodeBarang ).load();
	$("#totalPenjualan").load("data.php?totalPenjualanFilter=faktur&startDate="+startDate+"&endDate="+endDate+"&status="+status+"&user_id="+user_id+"&pelanggan_id="+pelanggan_id+"&lapak_id="+lapak_id);
	//alert(status);
} );

});
</script>
