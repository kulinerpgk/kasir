<?php 

if(isset($_GET['tableKasir'])){
doTableJSON($_GET['tableKasir'],array(
"id",
"kode_barang",
"nama_barang",
"harga",
"qty",
"total",
"faktur",
"id_barang",
"date"
),"WHERE status=1 AND session='".userID($_SESSION['user'])."'");
}

elseif(isset($_GET['stokCart'])){
$items=doTableArray("daftar_barang",array("stok"),"WHERE id_barang='".$_GET['stokCart']."'");
echo $items[0][0];

}

elseif(isset($_GET['kasirBarang'])){
	$data_barang=array(
		"id_barang",
		"kode_barang",
		"nama_barang",
		"harga_jual",
		"stok",
		"satuan",
		"kategori_barang",
		"stok_minimal"
	);
	
	$where='';
	$order_by=array("id_barang","desc");	
	//echo doDatatables("daftar_barang",$data_barang,$where,$order_by,array("harga_beli","harga_jual"));
	doTableJSON('daftar_barang',$data_barang);
}

elseif(isset($_GET['totalSum'])){
$total= sumData($_GET['totalSum'],'total',"WHERE status=1 AND session='".userID($_SESSION['user'])."'");
echo'<div class="input-group" >
<span class="input-group-btn"><button class="btn " type="button" style="font-size:20px;">TOTAL  </button></span>
<input id="totalGrandKasir" style="border:2px solid #ccc;background:#f7f774;width:100%;font-size:25px;text-align:right" readonly value="'.currency($total).'"></div>';

}
elseif(isset($_GET['totalSumBayar'])){
$total= sumData($_GET['totalSumBayar'],'total',"WHERE status=1 AND session='".userID($_SESSION['user'])."'");
echo'<input class="form-control"  id="totalBayar"  readonly value="'.$total.'">';
}
elseif(isset($_GET['hitungKembali'])){
$total=xCurrency($_GET['total']);


$voucher=xCurrency($_GET['voucher']);
$diskon=xCurrency($_GET['diskon']);
$diskon=$_GET['diskon'];
$diskon=$diskon > 0 && !empty($diskon) ? round(($diskon/100) * $total) : 0;
if($diskon==0 OR $diskon==""){
	if($voucher!=0 OR $voucher!=''){
		$total=$total-$voucher;
	}else{
		$total=$total;
	}
}else{
$total=$total-$diskon;
}

if($_GET['ongkir']!=0 OR $_GET['ongkir']!=""){
	
$ongkir=xCurrency($_GET['ongkir']);
$total=$total+$ongkir;
}
if($_GET['pajak']!=0 OR $_GET['pajak']!=""){
	
$pajak=xCurrency($_GET['pajak']);
$pajak=round(($pajak/100) * $total);
$total=$total+$pajak;
}

$dibayar=xCurrency($_GET['bayar']);
$kembali=$dibayar-$total;
//echo "<b>Rp. ".currency($kembali)."</b>";

if($kembali>=0){
	echo "<b>Rp. ".currency($kembali)."</b><input id='bayarKurang' style='display:none' value='0'>";
}else{
	echo "<b>Rp. ".currency($kembali)." <br><input id='bayarKurang' style='display:none' value='1'><span style='color:red' > PEMBAYARAN BELUM CUKUP!</span></b>";
}

}
elseif(isset($_GET['hitungTotal'])){
$total=xCurrency($_GET['total']);
$voucher=xCurrency($_GET['voucher']);
$diskon=$_GET['diskon'];
$diskon=$diskon > 0 && !empty($diskon) ? round(($diskon/100) * $total) : 0;
if($diskon==0 OR $diskon==""){
	if($voucher!=0 OR $voucher!=''){
		$total=$total-$voucher;
	}else{
		$total=$total;
	}
}else{
$total=$total-$diskon;
}
if($_GET['ongkir']!=0 OR$_GET['ongkir']!=""){

$ongkir=xCurrency($_GET['ongkir']);
$total=$total+$ongkir;
}
if($_GET['pajak']!=0 OR$_GET['pajak']!=""){

$pajak=xCurrency($_GET['pajak']);
$pajak=round(($pajak/100) * $total);
$total=$total+$pajak;
}
echo '<b>Rp.'.currency($total).'</b><input type="hidden" id="hitungGrandTotal" value="'.currency($total).'">';
}
elseif(isset($_GET['kembaliNol'])){
	echo "<b>Rp.0</b>";
}


elseif(isset($_GET['addCart'])){
	if(isset($_GET['status'])){$status=$_GET['status'];}else{$status=1;}
	$barang=doTableArray("daftar_barang",array("id_barang","nama_barang","harga_jual","harga_beli","stok","stok_minimal"),"where kode_barang='".$_GET['kode_barang']."'");
	$user_id=$_GET['user_id'];
	$id_barang=$barang[0][0];
	$nama_barang=$barang[0][1];
	$harga=$barang[0][2];
	$hpp=$barang[0][3];
	$checkCart=checkData($_GET['addCart'],"WHERE kode_barang='".$_GET['kode_barang']."' AND status=1 AND session='".$user_id."' ");
	$total=$harga*$_GET['qty'];
	$total_hpp=$hpp*$_GET['qty'];
	
	
	if($checkCart <1){

		$data=array(
			"id_barang"=>$id_barang,
			"kode_barang"=>$_GET["kode_barang"],
			"nama_barang"=>$nama_barang,
			"harga"=>$harga,
			"qty"=>$_GET['qty'],
			"faktur"=>$_GET['faktur'],
			"date"=>date("Y-m-d"),
			"total"=>$total,
			"status"=>$status,
			"hpp"=>$hpp,
			"total_hpp"=>$total_hpp,
			"session"=>$user_id,
		);
		dbInsert($_GET['addCart'],$data);


	}else{
		doUpdate($_GET['addCart'],"qty=qty+".$_GET['qty'].",total=total+".$total."","WHERE kode_barang='".$_GET['kode_barang']."' AND status=".$status." AND session='".$_GET['user_id']."'");
		doUpdate($_GET['addCart'],"total_hpp=hpp*qty","WHERE kode_barang='".$_GET['kode_barang']."' AND status=".$status." AND session='".$_GET['user_id']."'");
	}

}
elseif(isset($_GET['delCart'])){
doDelete($_GET['tableCart'],"WHERE id=".$_GET['delCart']."");
}

elseif(isset($_GET['updateSupplier'])){
doUpdate($_GET['updateSupplier'],
"
nama_supplier='".$_GET['nama_supplier']."',
alamat='".$_GET['alamat']."',
kota='".$_GET['kota']."',
no_hp='".$_GET['no_hp']."',
email='".xCurrency($_GET['email'])."',
website='".xCurrency($_GET['website'])."',
rekening='".$_GET['rekening']."'
",
"WHERE id='".$_GET['id']."'"
);
}

elseif(isset($_GET['updateCart'])){
$qty=$_GET['qty'];
$harga=xCurrency($_GET['harga']);
$total=intVal($harga)*intVal($qty);
$hpp=doTableArray("kasir_penjualan",array("hpp"),"where id='".$_GET['id']."'");
$hpp=intVal($hpp[0][0]);	
$total_hpp=intVal($hpp)*intVal($qty);

$data=array(
	"qty"=>$qty,
	"harga"=>$harga,
	"total"=>$total,
	"total_hpp"=>$total_hpp,
);
dbUpdate($_GET['updateCart'],$data,"WHERE id='".$_GET['id']."'");
print_r($hpp);
}
elseif(isset($_GET['plusCart'])){
$qty=$_GET['plusCart']+1;
$harga=xCurrency($_GET['harga']);
$total=$harga*$qty;


$data=array(
	"qty"=>$qty,
	"total"=>$total,
);
dbUpdate($_GET['modeCart'],$data,"WHERE id='".$_GET['id']."'");


doUpdate($_GET['modeCart'], //tabel:kasir_penjualan
"total_hpp=hpp*qty","WHERE id='".$_GET['id']."'"
);
}
elseif(isset($_GET['minusCart'])){
$qty=$_GET['minusCart']-1;
$harga=xCurrency($_GET['harga']);
$total=$harga*$qty;

$data=array(
	"qty"=>$qty,
	"total"=>$total,
);
dbUpdate($_GET['modeCart'],$data,"WHERE id='".$_GET['id']."'");

doUpdate($_GET['modeCart'], //tabel:kasir_penjualan
"total_hpp=hpp*qty","WHERE id='".$_GET['id']."'"
);
}

elseif(isset($_GET['printKasir'])){
$items=doTableArray("kasir_penjualan",array("kode_barang","nama_barang","harga","qty","total","faktur"),"WHERE status=2 AND faktur='".$_GET['faktur']."'");
$faktur=dbResult("faktur",array("date","pelanggan_id","pemasukan","diskon","voucher","dibayar","kembali","ongkir","pajak"),"where faktur='".$_GET['faktur']."'",true,true);
$faktur=(object)$faktur[0];
$date= $faktur->date;
$pelanggan_id= $faktur->pelanggan_id;
$pemasukan= $faktur->pemasukan;
$diskon= $faktur->diskon;
$voucher= $faktur->voucher;
$dibayar= $faktur->dibayar;
$kembali= $faktur->kembali;
$ongkir= $faktur->ongkir;
$pajak= $faktur->pajak;
if(intval($pelanggan_id) !='' || intval($pelanggan_id)!=0){
$supplier=doTableArray("pelanggan",array("nama_pelanggan"),"where id='".intval($pelanggan_id)."'");
$nama_pelanggan=$supplier[0][0];
}else{
$nama_pelanggan='-';
}


?>
<div>
    <div id="printArea">
        <table class="table">
            <tr>
                <td style="width:100px">Tanggal</td>
                <td style="width:30px">:</td>
                <td><?php echo $date;?></td>
                <td style="width:100px">Pelanggan</td>
                <td style="width:30px">:</td>
                <td><?php echo $nama_pelanggan;?></td>
            </tr>
            <tr>
                <td>Faktur</td>
                <td>:</td>
                <td><?php echo $_GET['faktur'];?></td>
                <td colspan=3></td>
            </tr>
        </table>

        <table class="table " id="dataPenjualan" cellspacing="0">
            <thead>
                <tr>
                    <th>Nama Barang</th>
                    <th>Harga</th>
                    <th>Qty</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
                <?php
  $i=1;
	foreach($items as $row){
	  echo '<tr>';
	  echo '<td>'.$row[1].'</td>';
	  echo '<td>'. currency($row[2]).'</td>';
	  echo '<td>'.$row[3].'</td>';
	  echo '<td>'. currency($row[4]).'</td>';
	  echo '</tr>';
	  $i++; 	  
  }   
  ?>

                <?php if(intval($voucher) != 0 || intval($voucher)!=''){?> <tr>
                    <td colspan=2>Voucher</td>
                    <td>:</td>
                    <td><?php echo currency(intval($voucher));?></td>
                </tr><?php } ?>
                <?php if(intval($diskon) != 0 || intval($diskon) !=''){?> <tr>
                    <td colspan=2>Diskon</td>
                    <td>:</td>
                    <td><?php echo currency(intval($diskon));?></td>
                </tr><?php } ?>
                <?php if(intval($ongkir) != 0 || intval($ongkir)!=''){?> <tr>
                    <td colspan=2>Ongkir</td>
                    <td>:</td>
                    <td><?php echo currency(intval($ongkir));?></td>
                </tr><?php } ?>
                <?php if(intval($pajak) != 0 || intval($pajak)!=''){?> <tr>
                    <td colspan=2>Pajak</td>
                    <td>:</td>
                    <td><?php echo currency(intval($pajak));?></td>
                </tr><?php } ?>
                <tr>
                    <td colspan=2>Total</td>
                    <td>:</td>
                    <td><?php echo currency($pemasukan);?></td>
                </tr>
                <tr>
                    <td colspan=2>Dibayar</td>
                    <td>:</td>
                    <td><?php echo  currency(intval ($dibayar));?></td>
                </tr>
                <tr>
                    <td colspan=2>Kembali</td>
                    <td>:</td>
                    <td><?php echo  currency(intval ($kembali));?></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<?php

}elseif(isset($_GET['loadFaktur'])){
	?>
<div class="input-group" style="margin-top:5px;width:100%">
    <span class="input-group-btn"><button class="btn " type="button">Faktur &nbsp;&nbsp;</button></span>
    <input class="form-control" type="text" id="faktur" name="faktur"
        value="<?php echo getFakturID('',$_GET['type']);?>">
</div>
<?php
	
}elseif(isset($_GET['loadDate'])){
	?>
<div class="input-group">
    <span class="input-group-btn"><button class="btn " type="button">Tanggal </button></span>
    <input class="form-control" type="text" id="date" value="<?php echo date("d/m/Y");?>">
</div>
<?php
	
}elseif(isset($_POST['addCart'])){


	if(isset($_POST['status'])){$status=$_POST['status'];}else{$status=1;}
	$barang=doTableArray("daftar_barang",array("id_barang","nama_barang","harga_jual","harga_beli","stok","stok_minimal"),"where kode_barang='".$_POST['kode_barang']."'");
	$user_id=$_POST['user_id'];
	$id_barang=$barang[0][0];
	$nama_barang=$barang[0][1];
	$harga=$barang[0][2];
	$hpp=$barang[0][3];
	$stok=$barang[0][4];
	$stok_minimal=$barang[0][5];
//	echo $harga;
	$checkCart=checkData($_POST['addCart'],"WHERE kode_barang='".$_POST['kode_barang']."' AND status=1 AND session='".$user_id."' ");
	$total=$harga*$_POST['qty'];
	$total_hpp=$hpp*$_POST['qty'];
	if($stok <= $stok_minimal)
	{
		echo 1;
		return false;
	}
	if($checkCart <1){

		$data=array(
			"id_barang"=>$id_barang,
			"kode_barang"=>$_POST["kode_barang"],
			"nama_barang"=>$nama_barang,
			"harga"=>$harga,
			"qty"=>$_POST['qty'],
			"faktur"=>$_POST['faktur'],
			"date"=>date("Y-m-d"),
			"total"=>$total,
			"status"=>$status,
			"hpp"=>$hpp,
			"total_hpp"=>$total_hpp,
			"session"=>$user_id,
		);
		dbInsert($_POST['addCart'],$data);
		

	}else{
		doUpdate($_POST['addCart'],"qty=qty+".$_POST['qty'].",total=total+".$total."","WHERE kode_barang='".$_POST['kode_barang']."' AND status=1 AND session='".$_POST['user_id']."'");
		doUpdate($_POST['addCart'],"total_hpp=hpp*qty","WHERE kode_barang='".$_POST['kode_barang']."' AND status=1 AND session='".$_POST['user_id']."'");
	}
//echo $_POST['kode_barang'];
}elseif(isset($_POST['inputBayarForm'])){
	
	if(isset($_POST['tempo']) && $_POST['tempo']!=''){
	$tempo=$_POST['tempo'];
	$getTempo=explode("/",$tempo);
	$tempo=$getTempo[2]."-".$getTempo[1]."-".$getTempo[0];
	}else{
	$tempo=0;	
	}

	$ekspedisi=isset($_POST['ekspedisi']) ? $_POST['ekspedisi'] : '';
	$user_id=$_POST['user_id'];
	$date=$_POST['date'];
	$getDate=explode("/",$date);
	$tanggal=$getDate[0];
	$bulan=$getDate[1];
	$tahun=$getDate[2];
	$date=$tahun."-".$bulan."-".$tanggal;
	
	$pelanggan_id=$_POST['pelanggan_id'];
	$faktur=$_POST['faktur'];
	$voucher=xCurrency($_POST['voucher']);

	$total=xCurrency($_POST['total']);


	$disc=xCurrency($_POST['diskon']);
	$diskon=xCurrency($_POST['diskon']);
	$dibayar=xCurrency($_POST['dibayar']);
	
	$diskon=round(($diskon/100) * $total);
	if($diskon==0 OR $diskon==""){
		if($voucher!=0 OR $voucher!=''){
			$grandTotal=$total-$voucher;
		}else{
			$grandTotal=$total;
		}
	}else{
		$grandTotal=$total-$diskon;
	}

	if($_POST['ongkir']!=0 OR $_POST['ongkir']!=""){
	$ongkir=xCurrency($_POST['ongkir']);
	$grandTotal=$grandTotal+$ongkir;
	}else{
	$ongkir=0;

	}

	if($_POST['pajak']!=0 OR $_POST['pajak']!=""){
	$pjk=isset($_POST['pajak']) ? xCurrency($_POST['pajak']) : '';
	$pajak=isset($_POST['pajak']) ? xCurrency($_POST['pajak']) : '';
	$pajak=round(($pajak/100) * $grandTotal);
	$grandTotal=$grandTotal+$pajak;
	}else{
	$pajak=0;
	}
	
	if($_POST['metode']=='tunai'){
	$status='tunai';
	$hutang=0;
	$hutang_dibayar=0;
	$hutang_sisa=0;
	}else{
	
	$status='kredit';
	$hutang=$grandTotal;
	$hutang_dibayar=$dibayar;
	$hutang_sisa=$grandTotal -  $dibayar;
	}
	$totalHPP=totalHPP($user_id);
	$kembali=$dibayar-$grandTotal;
	$pemasukan=$total-$diskon-$voucher+$pajak;
	$labarugi=$pemasukan-$totalHPP;

	$terjual= sumData('kasir_penjualan','qty',"WHERE status=1 AND session='".$user_id."'");

	$checkFaktur=checkData('faktur',"WHERE faktur='".$faktur."' ");
if($checkFaktur <1 )
{
	
			$data=array(
			"faktur"=>$faktur,
			"pelanggan_id"=>$pelanggan_id,
			"tanggal"=>$tanggal,
			"bulan"=>$bulan,
			"tahun"=>$tahun,
			"date"=>$date,
			"total"=>$total,
			"pemasukan"=>$pemasukan,
			"pengeluaran"=>0,
			"voucher"=>$voucher,
			"diskon"=>$diskon,
			"grand_total"=>$grandTotal,
			"dibayar"=>$dibayar,
			"kembali"=>$kembali,
			"mode"=>"penjualan",
			"keterangan"=>"penjualan ".$faktur,
			"status"=>$status,
			"hutang"=>$hutang,
			"hutang_dibayar"=>$hutang_dibayar,
			"hutang_sisa"=>$hutang_sisa,
			"tempo"=>$tempo,
			"user_id"=>$user_id,
			"disc"=>$disc,
			"pjk"=>$pjk,
			"pajak"=>$pajak,
			"ongkir"=>$ongkir,
			"ekspedisi"=>$ekspedisi,
			"total_hpp"=>$totalHPP,
			"laba_rugi"=>$labarugi,
			"terjual"=>$terjual,
			);
			dbInsert("faktur",$data);
		if($_POST['metode']=='kredit'){

			$data=array(
			"faktur"=>"DP.".date("ymdHis.").rand(1000,9999),
			"pelanggan_id"=>$pelanggan_id,
			"tanggal"=>$tanggal,
			"bulan"=>$bulan,
			"tahun"=>$tahun,
			"date"=>$date,
			"total"=>$total,
			"pemasukan"=>$dibayar,
			"pengeluaran"=>0,
			"voucher"=>0,
			"diskon"=>0,
			"grand_total"=>$grandTotal,
			"dibayar"=>$dibayar,
			"kembali"=>$kembali,
			"mode"=>"Bayar DP",
			"keterangan"=>"Bayar DP ".$faktur,
			"status"=>"tunai",
			"hutang"=>$hutang,
			"hutang_dibayar"=>$hutang_dibayar,
			"hutang_sisa"=>$hutang_sisa,
			"tempo"=>"",
			"user_id"=>$user_id,
			"disc"=>0,
			"pjk"=>$pjk,
			"pajak"=>0,
			"ongkir"=>0,
			"ekspedisi"=>"",
			"total_hpp"=>0,
			"laba_rugi"=>0,
			"terjual"=>0,
			);
			dbInsert("faktur",$data);
		}
		
		$items=doTableArray("kasir_penjualan",array("qty","id_barang"),"WHERE status=1 AND session='".$user_id."'");
		foreach($items as $row){
		//$isi=getBarang($row[2],"WHERE id_barang='".$row[1]."'");
		$qty=$row[0];

		doUpdate("daftar_barang", //tabel:kasir_penjualan
			"stok=stok-".$qty.",
			terjual=terjual+".$qty."
			",
			"WHERE id_barang='".$row[1]."'"
			);
		}
		
		doUpdate("kasir_penjualan", //tabel:kasir_penjualan
		"
		date='".$date."',
		faktur='".$faktur."',
		status='2'

		",
		"WHERE status='1' AND session='".$user_id."'"
		);
		
} //if faktur
			

}
?>