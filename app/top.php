<?php 
/* APLIKASI PENJUALAN DPOS PRO
 *
 * Framework DPOS BISNIS berbasis PHP
 *
 * Developed by djavasoft.com
 * Copyright (c) 2018, Djavasoft Smart Technology
 *
 * @author	Mohamad Anton Arizal
 * @copyright	Copyright (c) 2018 Djavasoft. (https://djavasoft.com/)
 *
 *
*/

$userLogin=$_SESSION['user'];
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title><?php echo getPengaturan('nama_toko');?> | <?php echo $SNAME;?> v.<?php echo $version;?></title>
    <link href="<?php echo $CORE_URL;?>/assets/adminLTE/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $CORE_URL;?>/assets/adminLTE/css/AdminLTE.min.css" rel="stylesheet">
    <link href="<?php echo $CORE_URL;?>/assets/adminLTE/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo $CORE_URL;?>/assets/adminLTE/css/_all-skins.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo $CORE_URL;?>/assets/adminLTE/css/ionicons.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo $CORE_URL;?>/assets/plugins/components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $CORE_URL;?>/assets/css/datatables.css" rel="stylesheet" type="text/css">
    <link href="<?php echo $CORE_URL;?>/assets/plugins/components/morris.js/morris.css" rel="stylesheet">
    <script src="<?php echo $CORE_URL;?>/assets/js/shortcuts.js"></script>
    <script src="<?php echo $CORE_URL;?>/assets/js/sweetalert2.all.js"></script>  
    <link href="<?php echo $CORE_URL;?>/assets/plugins/air-datepicker/css/datepicker.min.css" rel="stylesheet" type="text/css">
    <style>
.modal{overflow:auto!important}.skin-blue .main-header .navbar{_background-color:#3c8dbc;background:linear-gradient(to top,#0066ff 6%,#0099ff 100%)}.skin-blue .main-header .logo{_background-color:#367fa9;background:linear-gradient(to top,#0066ff 6%,#0066ff 100%);color:#fff;border-bottom:0 solid transparent}.content-wrapper{background-image:url("<?php echo $CORE_URL;?>/images/background/bg.jpg?t=<?php echo rand(); ?>");background-color:#eee;height:100%;background-position:center;background-repeat:no-repeat;background-size:cover;background-attachment:fixed}.modal-kasir{width:450px!important}.modal-header-success{color:#fff;background-color:#5cb85c}.modal-header-primary{color:#fff;background-color:#0c69d7}button{cursor:pointer}.modal-content{//min-height:600px}.nav-item :hover{background:#40474f}.datepicker{z-index:10000}table,tr,td{font-size:13px}.small-box h3{font-size:22px;font-weight:bold;margin:0 0 10px 0;white-space:nowrap;padding:0}#content_overlay{position:fixed;display:none;width:100%;height:100%;top:0;left:0;right:0;bottom:0;background-color:rgba(0,0,0,0.5);z-index:2000;cursor:pointer}#process_page{display:none;z-index:20000}
    </style>
    <script>
	var site_url 	= '<?=$CORE_URL?>';
	var user 		= '<?=$_SESSION["user"]?>';
	var user_id 	= '<?= userID($_SESSION['user']);?>';
    $('.content-wrapper').css('background', 'rgba(128, 128, 128, 0.2)');
    </script>
</head>

<body class="hold-transition skin-blue fixed sidebar-mini" onload="startbody()">


    <div class="wrapper">
        <header class="main-header">
            <a href="index.php" class="logo">
                <span class="logo-mini"><img src="images/icons/logo.png?t=<?=rand()?>" style="width:24px"></span>
                <span class="logo-lg"><img src="images/icons/logo.png?t=<?=rand()?>" style="width:24px"> <?php echo $SNAME;?></span>
            </a>

            <nav class="navbar navbar-static-top">
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>


                <div class="navbar-custom-menu">

                    <ul class="nav navbar-nav">
                        <li>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-user"></i> User : <?php echo $userLogin;?></a>
                            </a>
                        </li>
                        <li>
                            <a href="index.php?page=logout"> <i class="fa fa-sign-out"></i>Keluar</a>
                        </li>
                    </ul>
                </div>
            </nav>

        </header>