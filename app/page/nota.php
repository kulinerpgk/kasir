<?php 
/* APLIKASI PENJUALAN DPOS PRO
 *
 * Framework DPOS BISNIS berbasis PHP
 *
 * Developed by djavasoft.com
 * Copyright (c) 2018, Djavasoft Smart Technology
 *
 * @author	Mohamad Anton Arizal
 * @copyright	Copyright (c) 2008 Djavasoft. (https://djavasoft.com/)
 *
 *
*/

include'config.php';
function xdate($date){
	$date=explode('-',$date);	
	$date=$date[2].'-'.$date[1].'-'.$date[0];	
	return $date;
}
$myfile = fopen("app/page/setting_printer.php", "r") or die("Unable to open file!");
$data= fread($myfile,filesize("app/page/setting_printer.php"));
$set_print=json_decode($data, true);
fclose($myfile);
	$items=dbResult("kasir_penjualan",array("kode_barang","nama_barang","harga","qty","total","faktur"),"WHERE status=2 AND faktur='".$_GET['faktur']."'");
	$faktur=dbResult("faktur",array("date","pelanggan_id","total","pemasukan","diskon","voucher","dibayar","kembali","user_id","disc","pajak","pjk"),"where faktur='".$_GET['faktur']."'");
	
	$faktur=(object)$faktur[0];
	$date= $faktur->date;
	$pelanggan_id= $faktur->pelanggan_id;
	$total= $faktur->total;
	$pemasukan= $faktur->pemasukan;
	$diskon= $faktur->diskon;
	$voucher= $faktur->voucher;
	$dibayar= $faktur->dibayar;
	$kembali= $faktur->kembali;
	$pajak= $faktur->pajak;
	$disc= $faktur->disc;
	$pjk= $faktur->pjk;
	$userName= userName($faktur->user_id);

/* 	$date= $faktur[0][0];
	$pelanggan_id= $faktur[0][1];
	$total= $faktur[0][2];
	$pemasukan= $faktur[0][3];
	$diskon= $faktur[0][4];
	$voucher= $faktur[0][5];
	$dibayar= $faktur[0][6];
	$kembali= $faktur[0][7];
	$userName= userName($faktur[0][8]);
	$disc= $faktur[0][9];
	$pajak= $faktur[0][10];
	$pjk= $faktur[0][11];
*/

	if(intval($pelanggan_id) !='' || intval($pelanggan_id)!=0){
	$supplier=dbResult("pelanggan",array("nama_pelanggan"),"where id='".intval($pelanggan_id)."'");
	$nama_pelanggan=$supplier[0]["nama_pelanggan"];
	}else{
	$nama_pelanggan='-';
	}
	$font_size= !empty($set_print["font_size"]) ? $set_print["font_size"] : '12';
	?>
<html moznomarginboxes="" mozdisallowselectionprint=""
    style="margin-top:<?=$set_print["margin_top"];?>mm;margin-left:<?=$set_print["margin_left"];?>mm;">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title> <?php echo getPengaturan('nama_toko');?> - Cetak Nota </title>
    <style type="text/css">
    pre {
        font-weight: bold;
		font-size:<?=$font_size;?>px;
    }

    @media print {
        @page {
            margin: 0mm;
            font-size: 8px;
        }
    }
    </style>
</head>

<body <?php echo isset($_GET["print"]) && $_GET["print"]==0 ? "" : 'onload="window.print();"';?>
    cz-shortcut-listen="true">
    <?php 
if($_GET['ukuran']=='58'){?>
    <style type="text/css">
    body {
        width: <?=$_GET['ukuran'];
        ?>mm
    }
    </style>
    <p style="font-size:12px;font-family:arial;text-align:center">
        <?php if(file_exists('images/system/logo.png') AND $set_print["show_logo"]==1)
{
echo  '<img src="images/system/logo.png?t='.rand().'" style="margin:3px;width:50px;height:50px"><br>';
} ?>
        <b><?php echo getPengaturan('nama_toko');?></b><br><?php echo nl2br(getPengaturan('alamat'));?> <br>Telp.
        <?php echo getPengaturan('no_hp');?>

    </p>
    <pre>
---------------------------------
<?php echo xDate($date);?>		<?php echo date("H:i:s");?><br><?php echo $_GET['faktur'];?>	<?php echo $userName;?>
<br>---------------------------------<br> Harga	Qty	Total<br>---------------------------------
<?php
$i=1;
foreach($items as $row){
$row=(object) $row;	
echo ''.$row->nama_barang.'<br>';
echo ' '. currency($row->harga).'';
echo ' x ';
echo ''.$row->qty.'	';
echo 'Rp.'. currency($row->total).'<br>';
$i++; 	  
}   
?>
---------------------------------
<?php if(intval($diskon)!=0 OR intval($diskon)!=''){?>Harga	:	Rp.<?php echo currency($total);?><br>Diskon	:	Rp.<?php echo currency($diskon);?>(<?php echo $disc;?>%)<?php } ?><?php if(intval($voucher)!=0 OR intval($voucher)!=''){?><br>Voucher	:	Rp.<?php echo currency($voucher);?><?php } ?><?php if(intval($pjk)!=0 OR intval($pjk)!=''){?><br>Pajak	:	Rp.<?php echo currency($pajak);?>(<?php echo $pjk;?>%)<?php } ?><br>Total	:	Rp.<?php echo currency($pemasukan);?><br>Bayar	:	Rp.<?php echo currency($dibayar);?><br>	-------------------------<br>Kembali	:	Rp.<?php echo currency($kembali);?><br>=================================
<center><?=$set_print["tulisan_footer"];?></center>
<BR>
</pre>
    <?php } elseif($_GET['ukuran']=='80'){
/////////////////////////////////////// UKURAN 80MM ////////////////////////////////////////////////////////
?>
    <style type="text/css">
    body {
        width: <?=$_GET['ukuran'];
        ?>mm
    }
    </style>
    <p style="font-size:12px;font-family:arial;text-align:center">
        <?php if(file_exists('images/system/logo.png') AND $set_print["show_logo"]==1)
{
echo  '<img src="images/system/logo.png?t='.rand().'" style="margin:3px;width:50px;height:50px"><br>';
} ?>
        <b><?php echo getPengaturan('nama_toko');?></b><br><?php echo getPengaturan('alamat');?> <br>Telp.
        <?php echo getPengaturan('no_hp');?>

    </p>
    <pre>
--------------------------------------
<?php echo xDate($date);?>		<?php echo date("H:i:s");?><br><?php echo $_GET['faktur'];?>	<?php echo $userName;?>
<br>--------------------------------------<br> Harga		Qty	Total<br>--------------------------------------
<?php
$i=1;
foreach($items as $row){
	$row=(object) $row;
	echo ''.$row->nama_barang.'<br>';
	echo ' '. currency($row->harga).'';
	echo '	x ';
	echo ''.$row->qty.'	';
	echo 'Rp.'. currency($row->total).'<br>';
	$i++; 	  
}   
?>
--------------------------------------
 <?php if(intval($diskon)!=0 OR intval($diskon)!=''){?>Harga		:	Rp.<?php echo currency($total);?><br> Diskon		:		Rp.<?php echo currency($diskon);?>(<?php echo $disc;?>%)<?php }?><?php if(intval($voucher)!=0 OR intval($voucher)!=''){?><br> Voucher		:		Rp.<?php echo currency($voucher);?><?php } ?><?php if(intval($pjk)!=0 OR intval($pjk)!=''){?><br> Pajak		:	Rp.<?php echo currency($pajak);?>(<?php echo $pjk;?>%)<?php } ?><br> Total		:	Rp.<?php echo currency($pemasukan);?><br> Bayar		:	Rp.<?php echo currency($dibayar);?><br>		----------------------<br> Kembali	:	Rp.<?php echo currency($kembali);?><br>======================================
<center><?=$set_print["tulisan_footer"];?></center>

<BR>
</pre>

    <?php }elseif($_GET['ukuran']=='A4'){

	$font_size= '11';
		?>


    <link href="<?php echo $CORE_URL;?>/assets/adminLTE/css/bootstrap.min.css" rel="stylesheet">
    <style>
    table,
    tr,
    td {
        font-size: <?=$font_size;?>;
				padding:1px!important;

    }
    </style>
    <div class="title" style="margin-bottom:10px;text-align:left;margin-top:10px;margin-left:10px">
        <b><?php echo getPengaturan('nama_toko');?></b>
        <br>
        <small><?php echo getPengaturan('alamat');?> Telp/HP <?php echo getPengaturan('no_hp');?></small>
    </div>
    <table class="table">
        <tr>
            <td style="width:150px">Tanggal</td>
            <td style="width:30px">:</td>
            <td><?php echo $date;?> <?php echo date('H:i:s');?> </td>
            <td>Pelanggan</td>
            <td style="width:30px">:</td>
            <td><?php echo $nama_pelanggan;?></td>
        </tr>
        <tr>
            <td style="width:150px">Faktur Penjualan</td>
            <td>:</td>
            <td><?php echo $_GET['faktur'];?></td>
            <td style="width:150px"></td>
            <td></td>
            <td></td>
        </tr>
    </table>

    <table class="table table-condensed" id="dataPenjualan" cellspacing="0">
        <thead>
            <tr>
                <th style="width:50px">No</th>
                <th>Kode</th>
                <th>Nama Barang</th>
                <th>Harga</th>
                <th>Qty</th>
                <th>Total</th>
            </tr>
        </thead>
        <tbody>
            <?php
  $i=1;
	foreach($items as $row){
		$row=(object) $row;	
		echo '<tr>';
		echo '<td>'.$i.'</td>';
		echo '<td>'.$row->kode_barang.'</td>';
		echo '<td>'.$row->nama_barang.'</td>';
		echo '<td>'. currency($row->harga).'</td>';
		echo '<td>'.$row->qty.'</td>';
		echo '<td>'. currency($row->total).'</td>';
		echo '</tr>';
		$i++; 	  
  }   
  ?>
            <?php if(intval($voucher) != 0 || intval($voucher)!=''){?> <tr>
                <td colspan=3></td>
                <td colspan=2><b>Voucher</td>
                <td> : <?php echo currency(intval($voucher));?></td>
            </tr><?php } ?>
            <?php if(intval($diskon) != 0 || intval($diskon)!=''){?> <tr>
                <td colspan=3></td>
                <td colspan=2><b>Diskon</td>
                <td> : <?php echo currency(intval($diskon));?></td>
            </tr><?php } ?>
            <tr>
                <td colspan=3></td>
                <td colspan=2><b>Total</b></td>
                <td> : <?php echo currency($pemasukan);?></td>
            </tr>
            <tr>
                <td colspan=3></td>
                <td colspan=2><b>Dibayar</b></td>
                <td> : <?php echo  currency(intval ($dibayar));?></td>
            </tr>
            <tr>
                <td colspan=3></td>
                <td colspan=2><b>Kembali</b></td>
                <td> : <?php echo  currency(intval ($kembali));?></td>
            </tr>
        </tbody>
    </table>
    <?php } ?>
</body>

</html>