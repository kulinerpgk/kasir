<script>
var version = '<?=$version?>';
var sname = '<?=$SNAME?>';
var toko = '<b><?php echo getPengaturan('nama_toko');?></b>';
var alamat = '<?php echo str_replace("\n","",nl2br(getPengaturan('alamat')));?>'
$("#toko").html('<div class="pull-right" id="versi"></div>'+toko+''+'<br><small>'+alamat+'</small>');
$("#versi").html("<span style='color:red;padding:5px;background:yellow;'>Software Version : " + version + " </span> <br><a class='btn btn-danger btn-sm' style='width:100%;margin-top:5px' id='go_activation' onclick='loadPage(\"aktivasi\")'>UPDATE Impessa</a>");
$("#extra").html('<b><?php echo SNAME();?> <?php echo version();?></b> Copyright &copy; <a href="https://impessa.co.id/">impessa.co.id</a> 2018 - <?=date("Y")?>');
function loadPage(page) {
    swal({
        title: '',
        html: '<img src="images/system/ajax-loader.gif"> <br> Memuat..',
        showCancelButton: false,
        showConfirmButton: false
    })
    $("#loadBody").load("load.php?mode=" + page, function() {
            swal.close();
        }
    );
    document.title = ucwords(page) + ' | <?php echo getPengaturan('nama_toko');?> ';
    var getTitle = $(document).attr('title');
    if (page != 'dashboard') {
        $("#header-toko").hide();
    } else {
        $("#header-toko").show();

    }
}
</script>
