<?php

// get latest german WordPress file
$ch = curl_init();
$source = "https://de.wordpress.org/latest-de_DE.zip";
curl_setopt($ch, CURLOPT_URL, $source);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
$data = curl_exec ($ch);
curl_close ($ch);

// save as wordpress.zip
$destination = "wordpress.zip"; 
$file = fopen($destination, "w+");
fputs($file, $data);
fclose($file);

echo " wordpress.zip downloaded; ";

// unzip
$zip = new ZipArchive;
$res = $zip->open('wordpress.zip');
if ($res === TRUE) {
    $zip->extractTo('.'); // directory to extract contents to
    $zip->close();
    echo ' wordpress.zip extracted; ';
    unlink('wordpress.zip');
    echo ' wordpress.zip deleted; ';
} else {
    echo ' unzip failed; ';
}